#!python3
"""
Created: Sep 18 2023

@author: wadejenkins
e2w_sched is a utility to build proposed schedules for an OnCall (engaged to wait) rotation

LilyPy uses a yaml configuration file to set parameters for the rotation, such as team members, number of shifts, start/end dates, etc
"""
import argparse
import yaml

#load_piece_config reads the configuration and sets global variables for the project
#this function is light on error checking at the moment
def build_config(config_filename):
    ##Don't have any error checking or conditions here yet, but simple loading is fine
    try:
        f = open(config_filename);
    except FileNotFoundError:
        print("Config file not found");
        exit();
    config = yaml.safe_load(f);
#    global _OCTAVES;
#    global _PITCHES;
#    global _TIME_NUM;
#    global _TIME_DENOM;
#    global _KEY;
#    global _MODE;
#    global _CLEF;
#    global _LIMIT;
#    global _NUMPITCH;
#    _TIME_NUM=config['FrontMatter']['TimeNum']
#    _TIME_DENOM=config['FrontMatter']['TimeDenom']
#    _KEY=config['FrontMatter']['Key']
#    _MODE=config['FrontMatter']['Mode']
#    _CLEF=config['FrontMatter']['Clef']
#    _LIMIT = config['Instrument']['LimitPitches'];
#    _NUMPITCH = config['Instrument']['PitchLimit'];
#    determine_pitch_info(config);




###Main
parser = argparse.ArgumentParser(description='On-Call Schedule Builder')
parser.add_argument("--file","-f", default="e2w.yaml")
parser.add_argument("--config-only","-c", default=False)
parser.add_argument("--print-config","-p", default=False)

args=parser.parse_args()
#print(args.file)
build_config(args.file)
